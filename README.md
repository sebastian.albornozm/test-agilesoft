# Backend test for agilesoft
# Statement

Se requiere desarrollar una API Rest para un sistema to-do list con las siguientes funcionalidades:
- Obtener listado de tareas y su estado
- Agregar tarea
- Marcar tarea como resuelta


Los datos de la tarea son:
- Nombre
- Estado (resuelto/no resuelto)
- Descripción

hints: 
- Uso apropiado del framework, documentación y calidad del codigo
- No se requiere utilizar una base de datos para realizar el ejercicio. Puede mantener los datos en memoria.

## Solution
#### Language & framework
- Typescript
- Express
#### Install
`npm install`
#### Scripts
```
npm run dev
npm run build
npm start
```
- `dev`: Starts project on watch mode
- `build`: Compile project
- `start`: Starts project builded in javascript

#### Endpoints
###### url/todo/createTask
method = POST

request object
```json
{
	"name":"task1",
	"description":"first task to be commited"
}
```
response object
```json
{
  "task": {
    "name": "task1",
    "status": false,
    "description": "first task to be commited"
  }
}
```
###### url/todo/getList
method = GET

response object
```json
{
  "listado": [
    {
      "name": "task1",
      "status": false,
      "description": "first task to be commited"
    },
    {
      "name": "task2",
      "status": false,
      "description": "second task to be commited"
    }
  ]
}
```
###### url/todo/resolveTask?task=1
method = GET

response object
```json
{
  "task": {
    "name": "task1",
    "status": true,
    "description": "first task to be commited"
  }
}
```
Developed by Sebatian Albornoz Medina