import Server from './server';
import { ENV } from './config/env.config';
import { Todo } from './services/todo/Todo';

export const server = Server.init(parseInt(ENV.PORT));

export let listaToDo = Array<Todo>();

server.start(() => {
  console.log("Server running @ port " + server.port);
});