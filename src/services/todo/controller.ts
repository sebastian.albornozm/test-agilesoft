import { Todo } from './Todo';
import { listaToDo } from '../../app';
 
import { Request, Response } from 'express';

export class TodoController {
  
  public static async createTask(req:Request, res:Response){
    try{
      let body = req.body;
      let task = new Todo(body.name, false, body.description);
      listaToDo.push(task);
      res.status(200).json({list: listaToDo})
    } catch (err) {
      console.log("error " + err);
    }
  }

  public static async getListado(req:Request, res:Response) {  
    try {
      res.status(200).json({ listado: listaToDo});
    } catch(err) {
      console.log(err);
    }

  }

  public static async resolveTask(req:Request, res:Response) {
    try{
      let taskIndex = req.query.task as string;
      let indice: number = parseInt(taskIndex);
      listaToDo[indice-1].setStatus(true);
      res.status(200).json({task : listaToDo[indice-1]})
    } catch(err){
      console.log(err);
    }
  }
}