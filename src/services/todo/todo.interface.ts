export interface todo{
  getName(): string,
  getStatus(): boolean,
  getDescription(): string
}