import { todo } from './todo.interface';

export class Todo implements todo{ 
  
  private name: string;
  private status: boolean;
  private description: string;

  constructor(name: string, status: boolean, description: string){ 
    this.name = name;
    this.status = status;
    this.description = description;
  }
  
  getName(): string{
    return this.name;
  }
  getStatus(): boolean{
    return this.status
  }
  getDescription(): string{
    return this.description;
  }
  setStatus(status: boolean) {
    this.status = status;
  }
}