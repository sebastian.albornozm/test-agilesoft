import { Router } from 'express';
import { TodoController } from './controller';

const router = Router();

router.post('/createTask', TodoController.createTask);
router.get('/getList', TodoController.getListado);
router.get('/resolveTask', TodoController.resolveTask);

export default router;