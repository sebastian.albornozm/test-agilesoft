import express from 'express';
import cors from 'cors';
import todoRoutes from './services/todo/routes';

export default class Server{
  
  public app: express.Application;
  public port: number;

  constructor(port: number)  {
      this.port = port,
      this.app = express();
  }

  static init(port: number)  {
    return new Server(port);    
  }

  config():void {
    this.app.use(cors());
    this.app.use(express.json());
    this.app.use("/todo", todoRoutes);
  }

  start(callback: Function)  {
    this.config();
    this.app.listen(this.port, callback());
  }
}
