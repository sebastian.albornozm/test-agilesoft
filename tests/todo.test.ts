import { Todo } from './../src/services/todo/Todo';

describe('Todo class', function() {

  let obj = new Todo("test task", false, "test description");

  it('should create a To do object and should get the name', function () {
    expect(obj.getName()).toBe("test task");
  });
  
  it('should return false on a new object', function () {
    expect(obj.getStatus()).toBe(false);
  });

  it('should print the description of the task', function () {
    expect(obj.getDescription()).toBe("test description");
  });

  it('should be type of Todo object', function () {
    expect(obj).toBe(typeof Todo);
  });

});